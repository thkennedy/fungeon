﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SLS.Widgets.Table;
using System;
using Parse;
using System.Collections.Generic;

public enum DungeonType
{

}

public class DungeonGridDisplay : MonoBehaviour
{
    private Table dungeonGrid;
    IEnumerable<ParseObject> dungeonsList = null;
    bool gridIsDirty = false;

    // Use this for initialization
    void Start()
    {
        dungeonGrid = GetComponent<Table>();

        dungeonGrid.reset();

        dungeonGrid.addTextColumn("Dungeon Name");
        dungeonGrid.addTextColumn("Dungeon Type");
        dungeonGrid.addTextColumn("Dungeon Level");

        dungeonGrid.initialize(OnTableSelected);

        

        // Draw Your Table
        this.dungeonGrid.startRenderEngine();

    }

    private void OnTableSelected(Datum arg1, Column arg2)
    {
        Debug.Log(arg1.ToString() + arg2.ToString());
    }

    public void OnRefreshDungeonsClicked()
    {
        dungeonGrid.data.Clear();
        var query = ParseObject.GetQuery("Dungeons").WhereEqualTo("DunState", "TX");

        query.FindAsync().ContinueWith(t =>
        {
            dungeonsList = t.Result;
            gridIsDirty = true;
        });
    }

    private void RefreshDungeonGrid()
    {

        int i = 1;
        Datum d = null;
        foreach (var dungeon in dungeonsList)
        {
            d = Datum.Body(i++.ToString());
            d.elements.Add(dungeon["DunName"].ToString());
            d.elements.Add(dungeon["DunType"].ToString());
            d.elements.Add(dungeon["DunLevel"].ToString());

            this.dungeonGrid.data.Add(d);
        }
    }

    void Update()
    {
        if (gridIsDirty)
        {
            RefreshDungeonGrid();
            gridIsDirty = false;

        }
    }

    
}
