﻿using UnityEngine;
using Parse;
using System.Collections;

public class Tester : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        ParseObject testObject = new ParseObject("TestObject");
        testObject["foo"] = "bar";
        testObject.SaveAsync();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
